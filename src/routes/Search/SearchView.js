import React from 'react';

import SearchContainer from './containers/SearchContainer';

const SearchView = () => (
  <div>
    <SearchContainer />
  </div>
);

export default SearchView;
