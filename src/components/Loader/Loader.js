import React from 'react';
import PropTypes from 'prop-types';

import styles from './Loader.scss';

const propTypes = {
  isBlocking:     PropTypes.bool.isRequired,
  isFullScreen:   PropTypes.bool.isRequired
};

const defaultProps = {
  isBlocking:     false,
  isFullScreen:   false
};

const Loader = ({ isFullScreen, isBlocking }) => (
  <div
    className={styles.beLoader +
      (isFullScreen ? ` ${styles.beLoaderFullscreen}` : '') +
      (isBlocking ? ` ${styles.beLoaderBlocking}` : '')
    }
  >
    <div className={styles.beLoaderSpinner}>
      <i /><i /><i /><i />
    </div>
    <span className={styles.srOnly}>Loading...</span>
  </div>
);

Loader.propTypes = propTypes;
Loader.defaultProps = defaultProps;

export default Loader;
