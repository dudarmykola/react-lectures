import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route, Switch } from 'react-router-dom';

import NotFoundView from 'routes/NotFound';

import Sub1View from './routes/Sub1';
import Sub2View from './routes/Sub2';
import Sub3View from './routes/Sub3';
import Sub4View from './routes/Sub4';

import SubRouteNavigation from 'components/SubRouteNavigation';

import styles from './AdminView.scss';

import { ADMIN_SUB1_URL, ADMIN_SUB2_URL, ADMIN_SUB3_URL, ADMIN_SUB4_URL } from 'consts';

const propTypes = {
  match:        PropTypes.shape({
    url:          PropTypes.string.isRequired,
    isExact:      PropTypes.bool.isRequired
  }).isRequired
};

const subRoutes = [
  {
    url: ADMIN_SUB1_URL,
    name: 'SubRoute 1',
    component: Sub1View
  },
  {
    url: ADMIN_SUB2_URL,
    name: 'SubRoute 2',
    component: Sub2View
  },
  {
    url: ADMIN_SUB3_URL,
    name: 'SubRoute 3',
    component: Sub3View
  },
  {
    url: ADMIN_SUB4_URL,
    name: 'SubRoute 4',
    component: Sub4View
  }
];

const AdminView = ({ match: { url, isExact } }) => (
  <div className={styles.beAdmin}>
    {isExact &&
      <Redirect to={`${url}${ADMIN_SUB1_URL}`} />
    }
    <div className={styles.beAdminHeader}>
      <SubRouteNavigation
        parentUrl={url}
        subRoutes={subRoutes}
      />
    </div>
    <Switch>
      {subRoutes.map(({ url: subUrl, component }) => (
        <Route
          key={subUrl}
          component={component}
          path={`${url}${subUrl}`}
        />
      ))}
      <Route
        component={NotFoundView}
      />
    </Switch>
  </div>
);

AdminView.propTypes = propTypes;

export default AdminView;
