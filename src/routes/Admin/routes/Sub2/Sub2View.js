import React, { Component } from 'react';

import styles from './Sub2View.scss';

class Sub2View extends Component {
  render () {
    return (
      <div className={styles.beAdminSub2}>
        Sub 2 View
      </div>
    );
  }
}

export default Sub2View;
