import React from 'react';
import PropTypes from 'prop-types';

const Checkmark = ({ style, fillClassName, fillColor }) => (
  <svg
    width='18'
    height='18'
    style={style}
    aria-hidden='true'
    viewBox='0 0 18 18'
    xmlns='http://www.w3.org/2000/svg'
  >
    <g fill='none'>
      <polygon points='0 0 18 0 18 18 0 18' />
      <polygon
        fill={fillColor}
        className={`be-icon-fill ${fillClassName || ''}`}
        points='6.8 12.1 3.6 9 2.6 10.1 6.8 14.3 15.8 5.3 14.7 4.2'
      />
    </g>
  </svg>
);

Checkmark.defaultProps = {
  fillColor           : '#9A9A9A'
};

Checkmark.propTypes = {
  style               : PropTypes.object,
  fillClassName       : PropTypes.string,
  fillColor           : PropTypes.string
};

export default Checkmark;
