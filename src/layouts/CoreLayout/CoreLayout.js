import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import Loader from 'components/Loader';
import Header from 'components/Header';

import 'styles/core.scss';
import styles from './CoreLayout.scss';

export const CoreLayout = ({ children, isFetching }) => (
  <div className={styles.appContainer}>
    {isFetching ?
      <Loader
        isBlocking
        isFullScreen
      />
      :
      <Fragment>
        <Header />
        <div className={styles.beMainContent}>
          {children}
        </div>
      </Fragment>
    }
  </div>
);

CoreLayout.propTypes = {
  children:     PropTypes.node,
  isFetching:   PropTypes.bool.isRequired
};

export default CoreLayout;
