import { MOCK_PROJECTS } from 'helpers/mocks';

export function getProjectsList ({ skip, limit }) {
  return new Promise(resolve => {
    setTimeout(() => resolve({
      items: MOCK_PROJECTS.slice(skip, skip + limit)
    }), 1000);
  });
}

export function deleteProject (id) {
  return new Promise(resolve => {
    setTimeout(resolve, 1000);
  });
}
