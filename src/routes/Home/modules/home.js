import * as api from './api';
import normalizeFetchReject from 'helpers/promises';

import { TABLE_FETCH_LIMIT } from 'consts';

// ------------------------------------
// Constants
// ------------------------------------

export const HOME_PROJECTS_LIST_GET_PENDING = 'HOME_PROJECTS_LIST_GET_PENDING';
export const HOME_PROJECTS_LIST_GET_REJECTED = 'HOME_PROJECTS_LIST_GET_REJECTED';
export const HOME_PROJECTS_LIST_GET_FULFILLED = 'HOME_PROJECTS_LIST_GET_FULFILLED';

export const HOME_PROJECTS_LIST_DELETE_PENDING = 'HOME_PROJECTS_LIST_DELETE_PENDING';
export const HOME_PROJECTS_LIST_DELETE_REJECTED = 'HOME_PROJECTS_LIST_DELETE_REJECTED';
export const HOME_PROJECTS_LIST_DELETE_FULFILLED = 'HOME_PROJECTS_LIST_DELETE_FULFILLED';

export const HOME_PROJECTS_LIST_UPDATE_OPTIONS = 'HOME_PROJECTS_LIST_UPDATE_OPTIONS';


// ------------------------------------
// Actions
// ------------------------------------

export function projectsListGetPending (skip) {
  return {
    type    : HOME_PROJECTS_LIST_GET_PENDING,
    payload : skip
  };
}

export function projectsListGetFullfiled (data = {}) {
  return {
    type    : HOME_PROJECTS_LIST_GET_FULFILLED,
    payload : data
  };
}

export function projectsListGetRejected (reason = '') {
  return {
    type    : HOME_PROJECTS_LIST_GET_REJECTED,
    payload : reason
  };
}

export function projectsListDeletePending () {
  return {
    type    : HOME_PROJECTS_LIST_DELETE_PENDING
  };
}

export function projectsListDeleteFullfiled (id) {
  return {
    type    : HOME_PROJECTS_LIST_DELETE_FULFILLED,
    payload : id
  };
}

export function projectsListDeleteRejected (reason = '') {
  return {
    type    : HOME_PROJECTS_LIST_DELETE_REJECTED,
    payload : reason
  };
}

export function projectsListUpdateOptions (options) {
  return {
    type    : HOME_PROJECTS_LIST_UPDATE_OPTIONS,
    payload : options
  };
}

// ------------------------------------
// Specialized Action Creator
// ------------------------------------

export const deleteProject = id => dispatch => {
  dispatch(projectsListDeletePending());
  api.deleteProject(id)
    .then(() => dispatch(projectsListDeleteFullfiled(id)))
    .catch(normalizeFetchReject)
    .catch(msg => dispatch(projectsListDeleteRejected(msg)));
};

export const getProjectsList = (isLoadMore = false) =>
  async (dispatch, getState) => {
    const { limit, skip } = getState().home.projectsList;
    const newSkip = isLoadMore ? skip + TABLE_FETCH_LIMIT : 0;

    dispatch(projectsListGetPending(newSkip));
    try {
      dispatch(projectsListGetFullfiled({
        data: await api.getProjectsList({ skip: newSkip, limit }),
        isLoadMore
      }));
    } catch (e) {
      dispatch(projectsListGetRejected(await normalizeFetchReject(e)));
    }
  };

// ------------------------------------
// Reducer
// ------------------------------------


const initialState = {
  projectsList: {
    skip: 0,
    limit: TABLE_FETCH_LIMIT,
    isFetching: false,
    isFetched: false,
    reason: '',
    items: [],
    options: {
      page: 1,
      perPage: 5,
      sortField: 'startDate',
      isSortAscending: false
    },
    isDeleteFetching: false,
    deleteReason: ''
  }
};

export default function homeReducer (state = initialState, action) {
  switch (action.type) {
    case HOME_PROJECTS_LIST_GET_PENDING:
      state = {
        ...state,
        projectsList: {
          ...state.projectsList,
          skip: action.payload,
          isFetching: true
        }
      };
      break;
    case HOME_PROJECTS_LIST_GET_FULFILLED: {
      const { data: { items }, isLoadMore } = action.payload;

      state = {
        ...state,
        projectsList: {
          ...state.projectsList,
          isFetching: false,
          isFetched: true,
          isLoadMore: false,
          items: isLoadMore
            ? [...state.projectsList.items, ...items]
            : items
        }
      };
      break;
    }
    case HOME_PROJECTS_LIST_GET_REJECTED:
      state = {
        ...state,
        projectsList: {
          ...state.projectsList,
          isFetching: false,
          isFetched: false,
          reason: action.payload
        }
      };
      break;
    case HOME_PROJECTS_LIST_DELETE_PENDING:
      state = {
        ...state,
        projectsList: {
          ...state.projectsList,
          isDeleteFetching: true
        }
      };
      break;
    case HOME_PROJECTS_LIST_DELETE_FULFILLED:
      state = {
        ...state,
        projectsList: {
          ...state.projectsList,
          isDeleteFetching: false,
          items: state.projectsList.items.filter(i => i.id !== action.payload)
        }
      };
      break;
    case HOME_PROJECTS_LIST_DELETE_REJECTED:
      state = {
        ...state,
        projectsList: {
          ...state.projectsList,
          isDeleteFetching: false,
          deleteReason: action.payload
        }
      };
      break;
    case HOME_PROJECTS_LIST_UPDATE_OPTIONS:
      state = {
        ...state,
        projectsList: {
          ...state.projectsList,
          options: {
            ...state.projectsList.options,
            ...action.payload
          }
        }
      };
      break;
    default:
  }

  return state;
}
