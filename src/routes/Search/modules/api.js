import { MOCK_PROJECTS, MOCK_USERS } from 'helpers/mocks';

export function searchEntities (query) {
  const filterByQuery =
    item => item.name.toLowerCase().indexOf(query.toLowerCase()) !== -1;

  return new Promise(resolve => {
    setTimeout(() => resolve({
      projects: MOCK_PROJECTS.filter(filterByQuery),
      users: MOCK_USERS.filter(filterByQuery)
    }), 1000);
  });
}
