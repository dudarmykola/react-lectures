import React from 'react';
import PropTypes from 'prop-types';

import { NavLink } from 'react-router-dom';


const propTypes = {
  userRoles:         PropTypes.array.isRequired,
  allowedRoles:     PropTypes.array
};

const ProtectedNavLink = ({ userRoles, allowedRoles, ...rest }) => (
  !allowedRoles || userRoles.some(role => allowedRoles.indexOf(role) !== -1)
    ? <NavLink {...rest} />
    : null
);

ProtectedNavLink.propTypes = propTypes;

export default ProtectedNavLink;
