import React from 'react';
import PropTypes from 'prop-types';

import styles from './EmptyState.scss';

const EmptyState = ({ infoIcon: Icon, caption, message }) => (
  <div className={styles.beEmptyPlaceholder}>
    <div className={styles.beEmptyPlaceholderIcon}>
      <Icon />
    </div>
    <p>{caption}</p>
    <p>{message}</p>
  </div>
);

EmptyState.propTypes = {
  infoIcon            : PropTypes.any.isRequired,
  caption             : PropTypes.string,
  message             : PropTypes.string
};

export default EmptyState;
