import React from 'react';
import PropTypes from 'prop-types';

const TableIcon = ({ style, fillClassName, fillColor }) => (
  <svg
    width='84'
    height='64'
    style={style}
    aria-hidden='true'
    viewBox='0 0 69 51'
    xmlns='http://www.w3.org/2000/svg'
  >
    <g opacity='.5'>
      <path
        fill={fillColor}
        fillRule='nonzero'
        className={`be-icon-fill ${fillClassName || ''}`}
        d='M2.76 11.716v33.77a2.758 2.758 0 0 0 2.76 2.757h57.96a2.758 2.758 0 0 0 2.76-2.757v-33.77H2.76zM5.52 0h57.96A5.517 5.517 0 0 1 69 5.514v39.972A5.517 5.517 0 0 1 63.48 51H5.52A5.517 5.517 0 0 1 0 45.486V5.514A5.517 5.517 0 0 1 5.52 0z'
      />
      <path
        fill={fillColor}
        fillRule='evenodd'
        transform='translate(1, 10)'
        className={`be-icon-fill ${fillClassName || ''}`}
        d='M51.804 28.053H67v2.736H51.804V39h-2.763v-8.21H34.536V39h-2.763v-8.21H17.268V39h-2.763v-8.21H0v-2.737h14.505V21.21H0v-2.737h14.505v-6.842H0V8.895h14.505V0h2.763v8.895h14.505V0h2.763v8.895h14.505V0h2.763v8.895H67v2.737H51.804v6.842H67v2.737H51.804v6.842zm-2.763 0V21.21H34.536v6.842h14.505zm-17.268 0V21.21H17.268v6.842h14.505zm-14.505-9.58h14.505v-6.841H17.268v6.842zm17.268 0h14.505v-6.841H34.536v6.842z'
      />
    </g>
  </svg>
);

TableIcon.defaultProps = {
  fillColor           : '#757575'
};

TableIcon.propTypes = {
  style               : PropTypes.object,
  fillColor           : PropTypes.string,
  fillClassName       : PropTypes.string
};

export default TableIcon;
