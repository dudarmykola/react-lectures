import React from 'react';

import Clock from 'components/Clock';
import Button from 'components/controls/Button';

import styles from './TeamsView.scss';

const TeamsView = () => (
  <div className={styles.beTeam}>
    <div className={styles.beTeamHeader}>
      Teams
      <Button
        type='Primary'
        title='Add team'
        name='be-teams-add-team-btn'
      />
    </div>
    <Clock>
      {(min, sec) => (
        <div>
          {min > 9 ? min : `0${min}`} : {sec > 9 ? sec : `0${sec}`}
        </div>
      )}
    </Clock>
  </div>
);

export default TeamsView;
