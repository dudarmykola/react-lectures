import React, { Component } from 'react';

import styles from './Sub3View.scss';

class Sub3View extends Component {
  render () {
    return (
      <div className={styles.beAdminSub3}>
        Sub 3 View
      </div>
    );
  }
}

export default Sub3View;
