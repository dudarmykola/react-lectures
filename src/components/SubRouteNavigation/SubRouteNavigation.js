import React from 'react';
import PropTypes from 'prop-types';

import ProtectedNavLink from 'components/ProtectedNavLink';

import styles from './SubRouteNavigation.scss';

const propTypes = {
  subRoutes       : PropTypes.arrayOf(PropTypes.shape({
    url             : PropTypes.string.isRequired,
    name            : PropTypes.string.isRequired,
    allowedRoles    : PropTypes.array
  })).isRequired,
  parentUrl       : PropTypes.string.isRequired,
  userRoles       : PropTypes.array.isRequired
};

const SubRouteNavigation = ({ subRoutes, parentUrl, userRoles }) => (
  <div className={styles.beSubrouteNav}>
    {subRoutes.map(({ url, name, allowedRoles }) => (
      <ProtectedNavLink
        key={name}
        userRoles={userRoles}
        to={`${parentUrl}${url}`}
        allowedRoles={allowedRoles}
        className={styles.beSubrouteNavLink}
        activeClassName={styles.beSubrouteNavActiveLink}
      >
        {name}
      </ProtectedNavLink>
    ))}
  </div>
);

SubRouteNavigation.propTypes = propTypes;

export default SubRouteNavigation;
