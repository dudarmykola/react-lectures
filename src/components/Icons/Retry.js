import React from 'react';
import PropTypes from 'prop-types';

const Retry = ({ style, fillClassName, strokeClassName, fillColor }) => (
  <svg
    width='18'
    height='18'
    style={style}
    viewBox='0 0 18 18'
    xmlns='http://www.w3.org/2000/svg'
  >
    <g fill='none' fillRule='evenodd'>
      <path
        fill={fillColor}
        d='M13 6l-3.062-.212 2.032-2.765z'
        className={`be-icon-fill ${fillClassName || ''}`}
      />
      <path
        stroke={fillColor}
        d='M11.763 4.832A5 5 0 0 0 4 9a5 5 0 1 0 10 0'
        className={`be-icon-stroke ${strokeClassName || ''}`}
      />
    </g>
  </svg>
);

Retry.defaultProps = {
  fillColor           : '#F46969'
};

Retry.propTypes = {
  style               : PropTypes.object,
  fillClassName       : PropTypes.string,
  strokeClassName     : PropTypes.string,
  fillColor           : PropTypes.string
};

export default Retry;
