/* eslint-disable no-undefined */
import React from 'react';
import { shallow } from 'enzyme';

import PaginationControl from '../../PaginationControl';

describe('PaginationControl tests', () => {
  let props;
  let shallowPaginationControl;

  const mockOnChange = jest.fn();

  const paginationControl = () => {
    if (!shallowPaginationControl)
      shallowPaginationControl = shallow(
        <PaginationControl {...props} />
      );

    return shallowPaginationControl;
  };

  beforeEach(() => {
    props = {
      page: 1,
      pages: 1,
      onChange: mockOnChange
    };
    shallowPaginationControl = undefined;
  });

  describe('When PaginationControl renders with default props', () => {
    it('renders correctly', () => {
      expect(paginationControl().get(0)).toEqual(null);
    });
  });

  describe('When PaginationControl renders with different pages', () => {
    beforeEach(() => {
      props.pages = 20;
    });

    it('has default function for onChange', () => {
      props.onChange = undefined;
      paginationControl().find('Connect(IconButton)').at(0).prop('onClick')();
    });

    it('renders correctly on 1 page', () => {
      expect(paginationControl().find('Connect(IconButton)').at(0).prop('disabled')).toEqual(true);
      expect(paginationControl().find('WeekPickerArrow').at(0).prop('fillClassName')).toEqual('');

      expect(paginationControl().find('Connect(IconButton)').at(1).prop('disabled')).toEqual(false);
      expect(paginationControl().find('WeekPickerArrow').at(1).prop('fillClassName')).toEqual('mdrPaginationIcon');

      expect(paginationControl().find('.mdrPaginationPage').length).toEqual(6);
      expect(paginationControl().find('.mdrPaginationPage').at(0).hasClass('mdrPaginationPageActive')).toEqual(true);
      expect(paginationControl().find('.mdrPaginationPage').at(3).hasClass('mdrPaginationGap')).toEqual(true);
    });

    it('renders correctly on 10 page', () => {
      props.page = 10;

      expect(paginationControl().find('Connect(IconButton)').at(0).prop('disabled')).toEqual(false);
      expect(paginationControl().find('WeekPickerArrow').at(0).prop('fillClassName')).toEqual('mdrPaginationIcon');

      expect(paginationControl().find('Connect(IconButton)').at(1).prop('disabled')).toEqual(false);
      expect(paginationControl().find('WeekPickerArrow').at(1).prop('fillClassName')).toEqual('mdrPaginationIcon');

      expect(paginationControl().find('.mdrPaginationPage').length).toEqual(11);
      expect(paginationControl().find('.mdrPaginationPage').at(5).hasClass('mdrPaginationPageActive')).toEqual(true);
      expect(paginationControl().find('.mdrPaginationPage').at(2).hasClass('mdrPaginationGap')).toEqual(true);
      expect(paginationControl().find('.mdrPaginationPage').at(8).hasClass('mdrPaginationGap')).toEqual(true);
    });

    it('renders correctly on 10 page with extraCount 1', () => {
      props.extraCount = 1;
      props.page = 10;

      expect(paginationControl().find('Connect(IconButton)').at(0).prop('disabled')).toEqual(false);
      expect(paginationControl().find('WeekPickerArrow').at(0).prop('fillClassName')).toEqual('mdrPaginationIcon');

      expect(paginationControl().find('Connect(IconButton)').at(1).prop('disabled')).toEqual(false);
      expect(paginationControl().find('WeekPickerArrow').at(1).prop('fillClassName')).toEqual('mdrPaginationIcon');

      expect(paginationControl().find('.mdrPaginationPage').length).toEqual(7);
      expect(paginationControl().find('.mdrPaginationPage').at(3).hasClass('mdrPaginationPageActive')).toEqual(true);
      expect(paginationControl().find('.mdrPaginationPage').at(1).hasClass('mdrPaginationGap')).toEqual(true);
      expect(paginationControl().find('.mdrPaginationPage').at(5).hasClass('mdrPaginationGap')).toEqual(true);
    });

    it('renders correctly on 20 page', () => {
      props.page = 20;

      expect(paginationControl().find('Connect(IconButton)').at(0).prop('disabled')).toEqual(false);
      expect(paginationControl().find('WeekPickerArrow').at(0).prop('fillClassName')).toEqual('mdrPaginationIcon');

      expect(paginationControl().find('Connect(IconButton)').at(1).prop('disabled')).toEqual(true);
      expect(paginationControl().find('WeekPickerArrow').at(1).prop('fillClassName')).toEqual('');

      expect(paginationControl().find('.mdrPaginationPage').length).toEqual(6);
      expect(paginationControl().find('.mdrPaginationPage').at(5).hasClass('mdrPaginationPageActive')).toEqual(true);
      expect(paginationControl().find('.mdrPaginationPage').at(2).hasClass('mdrPaginationGap')).toEqual(true);
    });

    it('handles events correctly', () => {
      props.page = 10;

      paginationControl().find('Connect(IconButton)').at(0).prop('onClick')();
      expect(mockOnChange.mock.calls.length).toEqual(1);
      expect(mockOnChange.mock.calls[0][0]).toEqual(9);

      paginationControl().find('Connect(IconButton)').at(1).prop('onClick')();
      expect(mockOnChange.mock.calls.length).toEqual(2);
      expect(mockOnChange.mock.calls[1][0]).toEqual(11);

      paginationControl().find('.mdrPaginationPage').at(5).prop('onClick')();
      paginationControl().find('.mdrPaginationPage').at(2).prop('onClick')();
      expect(mockOnChange.mock.calls.length).toEqual(2);

      paginationControl().find('.mdrPaginationPage').at(0).prop('onClick')();
      expect(mockOnChange.mock.calls.length).toEqual(3);
      expect(mockOnChange.mock.calls[2][0]).toEqual(1);
    });
  });
});
