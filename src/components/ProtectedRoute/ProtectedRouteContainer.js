import { connect } from 'react-redux';

import ProtectedRoute from './ProtectedRoute';

const mapStateToProps = state => ({
  userRoles: state.user.userRoles
});

export default connect(mapStateToProps)(ProtectedRoute);
