import { connect } from 'react-redux';
import { setUserMenuState } from 'store/reducers/user';

import Header from './Header';

const mapDispatchToProps = {
  setUserMenuState
};

const mapStateToProps = state => ({
  user: state.user,
  location: state.router.location
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
