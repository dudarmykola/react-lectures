import { connect } from 'react-redux';

import SubRouteNavigation from './SubRouteNavigation';

const mapStateToProps = state => ({
  userRoles: state.user.userRoles,
  location: state.router.location
});

export default connect(mapStateToProps)(SubRouteNavigation);
