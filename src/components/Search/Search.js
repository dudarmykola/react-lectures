import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Table from 'components/Table';
import TextInput from 'components/controls/TextInput';
import EmptyState from 'components/EmptyState';
import ProjectsList from 'components/ProjectsList';
import { TableIcon } from 'components/Icons';

import styles from './Search.scss';

const USERS_LIST_HEADER_FIELDS = [
  {
    name: 'User Name',
    field: 'name'
  },
  {
    name: 'Country',
    field: 'country'
  },
  {
    name: 'Experience',
    field: 'experience'
  }
];

class Search extends Component {
  static propTypes = {
    query                   : PropTypes.string.isRequired,
    searchResults           : PropTypes.shape({
      users                   : PropTypes.array.isRequired,
      projects                : PropTypes.array.isRequired,
      isFetched               : PropTypes.bool.isRequired,
      isFetching              : PropTypes.bool.isRequired
    }).isRequired,
    searchEntities          : PropTypes.func.isRequired,
    searchQueryChange       : PropTypes.func.isRequired
  }

  constructor (props, ...rest) {
    super(props, ...rest);

    this.searchEntities = _.debounce(props.searchEntities, 500);
    this.searchQueryChange = this.searchQueryChange.bind(this);
  }

  searchQueryChange (value) {
    this.searchEntities(value);
    this.props.searchQueryChange(value);
  }

  renderExperience (item) {
    return `${item.experience} year${item.experience !== 1 ? 's' : ''}`;
  }

  render () {
    const {
      query,
      searchResults: {
        users,
        projects,
        isFetched,
        isFetching
      }
    } = this.props;

    return (
      <div className={styles.beSearch}>
        <TextInput
          width='100%'
          value={query}
          name='be-search-search-input'
          onChange={this.searchQueryChange}
          placeholder='Type anything to search'
        />
        {!(projects.length || users.length) &&
          <EmptyState
            infoIcon={TableIcon}
            caption='No results.'
            message='Please adjust your search query.'
          />
        }
        {!!projects.length &&
          <ProjectsList
            items={projects}
            caption='Projects'
            isSortable={false}
            isFetched={isFetched}
            isFetching={isFetching}
          />
        }
        {!!users.length &&
          <div className={styles.beSearchTable}>
            <div className={styles.beSearchTableCaption}>
              Users
            </div>
            <Table
              items={users}
              isSortable={false}
              headerFields={USERS_LIST_HEADER_FIELDS}
              renderexperience={this.renderExperience}
            />
          </div>
        }
      </div>
    );
  }
}

export default Search;
