import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import { store } from 'index';
import userReducer from './reducers/user';

export const makeRootReducer = asyncReducers => combineReducers({
  router: routerReducer,
  user: userReducer,
  ...asyncReducers
});

export const injectReducer = ({ key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return;

  store.asyncReducers[key] = reducer;
  store.replaceReducer(makeRootReducer(store.asyncReducers));
};

export default makeRootReducer;
