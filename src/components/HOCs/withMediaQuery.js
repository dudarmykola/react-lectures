import React, { Component } from 'react';

const withMediaQueryHOC = (query, name) => WrappedComponent => {
  const propName = name || 'isMediaQueryMatched';

  class withMediaQuery extends Component {
    constructor (...args) {
      super(...args);
      this.state = {
        [propName]: window.matchMedia(query).matches
      };

      this.matchMediaQuery = this.matchMediaQuery.bind(this);
    }

    matchMediaQuery (mq) {
      this.setState({
        [propName]: mq.matches
      });
    }

    componentDidMount () {
      this.mq = window.matchMedia(query);
      this.mq.addListener(this.matchMediaQuery);
    }

    componentWillUnmount () {
      this.mq.removeListener(this.matchMediaQuery);
    }

    render () {
      return (
        <WrappedComponent
          {...this.props}
          {...this.state}
        />
      );
    }
  }
  return withMediaQuery;
};

export default withMediaQueryHOC;
