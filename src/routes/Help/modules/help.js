import * as api from './api';
import normalizeFetchReject from 'helpers/promises';

// ------------------------------------
// Constants
// ------------------------------------

export const HELP_DOCS_FETCH_PENDING = 'HELP_DOCS_FETCH_PENDING';
export const HELP_DOCS_FETCH_REJECTED = 'HELP_DOCS_FETCH_REJECTED';
export const HELP_DOCS_FETCH_FULLFILED = 'HELP_DOCS_FETCH_FULLFILED';

// ------------------------------------
// Actions
// ------------------------------------

export function helpDocsPending () {
  return {
    type    : HELP_DOCS_FETCH_PENDING
  };
}

export function helpDocsFullfiled (info = {}) {
  return {
    type    : HELP_DOCS_FETCH_FULLFILED,
    payload : info
  };
}

export function helpDocsRejected (reason = '') {
  return {
    type    : HELP_DOCS_FETCH_REJECTED,
    payload : reason
  };
}

// ------------------------------------
// Specialized Action Creator
// ------------------------------------

export const getHelpDocs = () => dispatch => {
  dispatch(helpDocsPending());

  api.getHelpDocs()
    .then(info => dispatch(helpDocsFullfiled(info)))
    .catch(normalizeFetchReject)
    .catch(msg => dispatch(helpDocsRejected(msg)));
};

// ------------------------------------
// Reducer
// --------
const initialState = {
  isFetching: false,
  isFetched: false,
  reason: '',
  data: []
};

export default function helpReducer (state = initialState, action) {
  switch (action.type) {
    case HELP_DOCS_FETCH_PENDING:
      state = {
        ...state,
        isFetching: true
      };
      break;
    case HELP_DOCS_FETCH_FULLFILED:
      state = {
        ...state,
        isFetching: false,
        isFetched: true,
        data: action.payload
      };
      break;
    case HELP_DOCS_FETCH_REJECTED:
      state = {
        ...state,
        isFetching: false,
        reason: action.payload
      };
      break;
    default:
  }

  return state;
}
