import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import makeRootReducer from './makeRootReducer';
import loggerMiddleware from './loggerMiddleware';
import { routerMiddleware } from 'react-router-redux';

export const history = createHistory();

export default (initialState = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  // eslint-disable-next-line
  const devMiddleware = process.env.NODE_ENV === 'development'
    ? [loggerMiddleware]
    : [];
  const middleware = [thunk, routerMiddleware(history), ...devMiddleware];

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = [];

  let composeEnhancers = compose;

  // eslint-disable-next-line
  if (process.env.NODE_ENV === 'development') {
    const composeWithDevToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;

    if (typeof composeWithDevToolsExtension === 'function')
      composeEnhancers = composeWithDevToolsExtension;
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createStore(
    makeRootReducer(),
    initialState,
    composeEnhancers(
      applyMiddleware(...middleware),
      ...enhancers
    )
  );

  store.asyncReducers = {};

  if (module.hot)
    module.hot.accept('./makeRootReducer', () => {
      const reducers = require('./makeRootReducer').default; // eslint-disable-line global-require

      store.replaceReducer(reducers(store.asyncReducers));
    });


  return store;
};
