import React, { Component } from 'react';

import styles from './Sub4View.scss';

class Sub4View extends Component {
  render () {
    return (
      <div className={styles.beAdminSub4}>
        Sub 4 View
      </div>
    );
  }
}

export default Sub4View;
