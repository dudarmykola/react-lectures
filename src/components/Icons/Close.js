import React from 'react';
import PropTypes from 'prop-types';

const Close = ({ width, height, style, fillClassName, fillColor }) => (
  <svg
    style={style}
    width={width}
    height={height}
    aria-hidden='true'
    viewBox='0 0 24 24'
    xmlns='http://www.w3.org/2000/svg'
  >
    <g fill='none'>
      <polygon points='0 0 24 0 24 24 0 24' />
      <polygon
        fill={fillColor}
        className={`be-icon-fill ${fillClassName || ''}`}
        points='19 6.4 17.6 5 12 10.6 6.4 5 5 6.4 10.6 12 5 17.6 6.4 19 12 13.4 17.6 19 19 17.6 13.4 12'
      />
    </g>
  </svg>
);

Close.defaultProps = {
  width               : '24',
  height              : '24',
  fillColor           : '#9A9A9A'
};

Close.propTypes = {
  style               : PropTypes.string,
  width               : PropTypes.string.isRequired,
  height              : PropTypes.string.isRequired,
  fillClassName       : PropTypes.string,
  fillColor           : PropTypes.string.isRequired
};

export default Close;
