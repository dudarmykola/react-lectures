import * as api from './api';
import { asyncActionFactory } from 'helpers/promises';

// ------------------------------------
// Constants
// ------------------------------------

export const SEARCH_GET_PENDING = 'SEARCH_GET_PENDING';
export const SEARCH_GET_REJECTED = 'SEARCH_GET_REJECTED';
export const SEARCH_GET_FULFILLED = 'SEARCH_GET_FULFILLED';

export const SEARCH_QUERY_CHANGE = 'SEARCH_QUERY_CHANGE';

// ------------------------------------
// Actions
// ------------------------------------

export function searchGetPending (section) {
  return {
    type    : SEARCH_GET_PENDING,
    payload : section
  };
}

export function searchGetFullfiled (data = {}) {
  return {
    type    : SEARCH_GET_FULFILLED,
    payload : data
  };
}

export function searchGetRejected (section, reason = '') {
  return {
    type    : SEARCH_GET_REJECTED,
    payload : { section, reason }
  };
}

export function searchQueryChange (query) {
  return {
    type    : SEARCH_QUERY_CHANGE,
    payload : query
  };
}

// ------------------------------------
// Specialized Action Creator
// ------------------------------------

export const searchEntities =
  asyncActionFactory(
    api.searchEntities,
    searchGetPending,
    searchGetFullfiled,
    searchGetRejected,
    'searchResults'
  );

// ------------------------------------
// Reducer
// ------------------------------------


const initialState = {
  query: '',
  searchResults: {
    users: [],
    projects: [],
    isFetched: false,
    isFetching: false,
    reason: ''
  }
};

export default function searchReducer (state = initialState, action) {
  switch (action.type) {
    case SEARCH_GET_PENDING: {
      const section = action.payload;

      state = {
        ...state,
        [section]: {
          ...state[section],
          isFetching: true
        }
      };
      break;
    }
    case SEARCH_GET_FULFILLED: {
      const { users, projects } = action.payload;

      state = {
        ...state,
        searchResults: {
          ...state.searchResults,
          isFetched: true,
          isFetching: false,
          users,
          projects
        }
      };
      break;
    }
    case SEARCH_GET_REJECTED: {
      const { section, reason } = action.payload;

      state = {
        ...state,
        [section]: {
          ...state[section],
          isFetching: false,
          isFetched: false,
          reason
        }
      };
      break;
    }
    case SEARCH_QUERY_CHANGE:
      state = {
        ...state,
        query: action.payload
      };
      break;
    default:
  }
  return state;
}
