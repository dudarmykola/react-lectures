import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Loader from 'components/Loader';
import IconButton from 'components/controls/IconButton';

import styles from './Table.scss';

class Table extends Component {
  static propTypes = {
    items         : PropTypes.array.isRequired,
    actions       : PropTypes.arrayOf(PropTypes.shape({
      icon          : PropTypes.any.isRequired,
      onAction      : PropTypes.func.isRequired
    })),
    children      : PropTypes.any,
    sortField     : PropTypes.string,
    isLoading     : PropTypes.bool.isRequired,
    isSortable    : PropTypes.bool.isRequired,
    headerFields  : PropTypes.arrayOf(PropTypes.shape({
      name          : PropTypes.string.isRequired,
      field         : PropTypes.string.isRequired,
      style         : PropTypes.object
    })).isRequired,
    isSortAscending : PropTypes.bool,
    handleSortChange: PropTypes.func
  };

  static defaultProps = {
    items             : [],
    actions           : [],
    isLoading         : false,
    isSortable        : true,
    isSortAscending   : false,
    handleSortChange  : () => {}
  };

  onToggleSort (field) {
    const {handleSortChange, sortField, isSortAscending} = this.props;

    handleSortChange({
      sortField: field,
      isSortAscending: field === sortField ? !isSortAscending : true
    });
  }

  render () {
    const {
      items,
      actions,
      children,
      isLoading,
      sortField,
      isSortable,
      headerFields,
      isSortAscending
    } = this.props;
    const renderField = (item, field) => this.props[`render${field}`]
      ? this.props[`render${field}`](item)
      : item[field];

    return (
      <div
        className={
          styles.beTable +
          (isSortable ? ` ${styles.beTableSortable}` : '') +
          (!items.length ? ` ${styles.beTableEmpty}` : '')
        }
      >
        {children}
        {isLoading &&
          <Loader isBlocking />
        }
        {!!items.length &&
          <div className={styles.beTableContainer}>
            <table>
              <thead>
                <tr>
                  {headerFields.map((f, idx) => (
                    <td
                      key={idx}
                      style={f.style}
                      onClick={() => isSortable ? this.onToggleSort(f.field) : null}
                      className={
                        f.field === sortField
                          ? `${styles.beTableHeaderSorting} ${isSortAscending ? styles.beTableHeaderSortingAsc : ''}`
                          : ''
                      }
                    >
                      <div className={styles.beTableHeaderCell}>
                        {f.name}
                        {isSortable &&
                          <div className={styles.beTableHeaderCellArrow} />
                        }
                      </div>
                    </td>
                  ))}
                  {!!actions.length &&
                    <td className={styles.beTableActions} />
                  }
                </tr>
              </thead>
              <tbody>
                {items.map((item, index) => (
                  <tr key={index}>
                    {headerFields.map((f, idx) => (
                      <td
                        key={idx}
                        style={f.style}
                      >
                        {renderField(item, f.field)}
                      </td>
                    ))}
                    {!!actions.length &&
                      <td className={styles.beTableActions}>
                        <div className={styles.beTableActionsWrapper}>
                          {actions.map((action, i) => !action.isVisible || action.isVisible(item) ? (
                            <IconButton
                              key={i}
                              onClick={() => action.onAction(item)}
                            >
                              {action.icon}
                            </IconButton>
                          ) : null)}
                        </div>
                      </td>
                    }
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        }
      </div>
    );
  }
}

export default Table;
