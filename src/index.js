import React from 'react';
import ReactDOM from 'react-dom';

import App from 'components/App';
import createStore from './store/createStore';
import registerServiceWorker from './registerServiceWorker';

// ========================================================
// Store Instantiation
// ========================================================

const initialState = window.___INITIAL_STATE__;

export const store = createStore(initialState);

// ========================================================
// Render Setup
// ========================================================

ReactDOM.render(
  <App store={store} />,
  document.getElementById('root')
);

registerServiceWorker();

if (module.hot)
  module.hot.accept('components/App', () => {
    ReactDOM.render(
      <App store={store} />,
      document.getElementById('root')
    );
  });
