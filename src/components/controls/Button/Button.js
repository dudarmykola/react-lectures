import React from 'react';
import PropTypes from 'prop-types';

import styles from './Button.scss';

const Button = ({
  type,
  name,
  icon: Icon,
  title,
  style,
  onClick,
  disabled,
  className
}) => {
  const buttonContent = Icon
    ? (<div className={styles.beBtnWithicon}>
      <Icon
        fillClassName={styles[`beIconFill${type}`]}
        strokeClassName={styles[`beIconStroke${type}`]}
      />
      <div className={styles.beBtnTitle}>{title}</div>
    </div>)
    : title;

  return (
    <button
      style={style}
      onClick={onClick}
      disabled={disabled}
      id={`be-btn-${name}`}
      onMouseDown={e => e.preventDefault()}
      className={`${styles.beBtn} ${styles[`beBtn${type}`]} ${className ? ` ${className}` : ''}`}
    >
      {buttonContent}
    </button>
  );
};

Button.propTypes = {
  type            : PropTypes.oneOf(['Primary', 'Secondary', 'InlineLight', 'InlineDark']).isRequired,
  name            : PropTypes.string.isRequired,
  icon            : PropTypes.any,
  title           : PropTypes.string.isRequired,
  style           : PropTypes.object,
  onClick         : PropTypes.func,
  disabled        : PropTypes.bool.isRequired,
  className       : PropTypes.string
};

Button.defaultProps = {
  type            : 'Secondary',
  disabled        : false
};

export default Button;
