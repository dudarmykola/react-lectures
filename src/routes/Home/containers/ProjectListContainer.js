/* eslint-disable no-shadow, no-console */
import { createSelector } from 'reselect';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import ProjectsList from 'components/ProjectsList';
import {
  deleteProject,
  getProjectsList,
  projectsListUpdateOptions
} from 'routes/Home/modules/home';

import { PROJECT_STATUSES } from 'consts';

class ProjectsListContainer extends Component {
  static propTypes = {
    projectsList              : PropTypes.shape({
      skip                      : PropTypes.number.isRequired,
      limit                     : PropTypes.number.isRequired,
      items                     : PropTypes.array.isRequired,
      reason                    : PropTypes.string.isRequired,
      isFetched                 : PropTypes.bool.isRequired,
      isFetching                : PropTypes.bool.isRequired,
      isDeleteFetching          : PropTypes.bool.isRequired
    }).isRequired,
    visibleItems              : PropTypes.array.isRequired,
    deleteProject             : PropTypes.func.isRequired,
    getProjectsList           : PropTypes.func.isRequired,
    projectsListUpdateOptions : PropTypes.func.isRequired
  }

  componentDidMount () {
    const { getProjectsList, projectsList: { isFetching, isFetched } } = this.props;

    if (!isFetching && !isFetched) getProjectsList();
  }

  render () {
    const {
      visibleItems,
      deleteProject,
      getProjectsList,
      projectsListUpdateOptions,
      projectsList: {
        items,
        options,
        isFetched,
        isFetching,
        isDeleteFetching
      }
    } = this.props;

    return (
      <ProjectsList
        options={options}
        items={visibleItems}
        total={items.length}
        isFetched={isFetched}
        isFetching={isFetching}
        caption='Home project list'
        deleteProject={deleteProject}
        getProjectsList={getProjectsList}
        isDeleteFetching={isDeleteFetching}
        handlePageChange={page => projectsListUpdateOptions({page})}
        handleSortChange={options => projectsListUpdateOptions({...options, page: 1})}
      />
    );
  }
}

const getPriority = item =>
  PROJECT_STATUSES[item.status] && PROJECT_STATUSES[item.status].priority;

const sorters = {
  sortstatus: (itemA, itemB, isAscending) => {
    const priorityA = getPriority(itemA);
    const priorityB = getPriority(itemB);

    if (priorityA === priorityB) return 0;

    return (isAscending && priorityA < priorityB) || (!isAscending && priorityA > priorityB) ? -1 : 1;
  }
};

const sortItemsSelector = createSelector(
  state => state.items,
  state => state.options.sortField,
  state => state.options.isSortAscending,
  (items, sortField, isSortAscending) => {
    console.log('sortItemsSelector triggered');
    const sortDefault = (a, b) => {
      if (a[sortField] === b[sortField]) return 0;
      return (isSortAscending && a[sortField] < b[sortField]) || (!isSortAscending && a[sortField] > b[sortField]) ? -1 : 1;
    };
    const sortCustom = sorters[`sort${sortField}`]
      ? (a, b) => sorters[`sort${sortField}`](a, b, isSortAscending)
      : null;

    return [...items].sort(sortCustom || sortDefault);
  }
);

const paginateItemsSelector = createSelector(
  sortItemsSelector,
  state => state.options.page,
  state => state.options.perPage,
  (items, page, perPage) => {
    console.log('paginateItemsSelector triggered');
    const startIndex = perPage * (page - 1);
    const endIndex = perPage * page;

    return items.slice(startIndex, endIndex);
  }
);

const mapStateToProps = state => ({
  projectsList: state.home.projectsList,
  visibleItems: paginateItemsSelector(state.home.projectsList)
});

const mapDispatchToProps = {
  deleteProject,
  getProjectsList,
  projectsListUpdateOptions
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectsListContainer);
